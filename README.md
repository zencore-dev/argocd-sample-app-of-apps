# ArgoCD App of Apps Repository

Welcome to the internal ArgoCD App of Apps repository for the Zencore organization! This repository acts as the control center for deploying and managing essential applications within our Kubernetes clusters. It follows the GitOps methodology, allowing us to maintain a coherent and version-controlled infrastructure.

## Introduction

This repository employs ArgoCD, a GitOps-based continuous delivery tool for Kubernetes, to automate application deployments and configurations. ArgoCD ensures that our applications are always in sync with their desired state, enhancing reliability and efficiency.

## Repository Structure

### Key Applications

#### 1. **Root Application: argocd-root.yaml**

This pivotal application orchestrates the deployment of all other applications in this repository. It serves as the foundation upon which our entire infrastructure is built.

#### 2. **Tools Applications**

- **Cluster Secret Management: tools/cluster-secret-store.yaml**
  Manages secrets securely, integrating with Google Cloud Secret Manager, ensuring sensitive information remains confidential.

- **HTTP Routing for ArgoCD: tools/argocd-httproute.yaml**
  Configures HTTP routes for ArgoCD, enabling seamless communication and access control.

- **External Secrets Management: tools/external-secrets.yaml**
  Manages external secrets, integrating with external sources securely, enhancing our security posture.

- **ArgoCD Integration with External Secrets: tools/argocd-external-secrets.yaml**
  Integrates ArgoCD with external secrets, ensuring a unified secret management approach.

- **Certificate Management: tools/cert-manager-cluster-issuer.yaml**
  Manages SSL certificates for secure communication, integrating with Let's Encrypt for automated certificate issuance.

- **Cert-Manager Configuration: tools/cert-manager.yaml**
  Configures Cert-Manager, ensuring SSL certificates are automatically renewed and validated, enhancing security.

- **ArgoCD Backend Policies: tools/argocd-backend-policy.yaml**
  Defines backend policies for ArgoCD, ensuring optimal performance and secure communication.

- **Gateway API Configuration: tools/gateway-api.yaml**
  Configures the gateway API, enabling secure access and routing for applications.

#### 3. **Zencore Applications**

- **Zencore Codelabs Development Environment: zencore/codelabs-dev.yaml**
  Sets up the development environment for Zencore Codelabs, ensuring consistent and reliable deployments.

- **Zencore Codelabs Production Environment: zencore/codelabs-prod.yaml**
  Configures the production environment for Zencore Codelabs, ensuring stability and scalability.

#### 4. **ArgoCD Apps Helm Chart**

- **Helm Chart Configuration: argocd-apps/Chart.yaml**
  Helm chart configuration for deploying applications, providing a robust and scalable deployment mechanism.

## How to Contribute

Contributions to this repository are essential for maintaining the integrity and functionality of our applications. To contribute:

1. Create a feature branch based on the `main` branch.
2. Implement your changes and commit them.
3. Push your branch to the repository.
4. Create a merge request, detailing the changes and the purpose of the merge.
5. Engage in the review process, addressing feedback and suggestions.
6. Once approved, your changes will be merged, ensuring a seamless integration into our infrastructure.

Your contributions are invaluable, and they help us enhance the reliability and security of our applications. Should you have any questions or need assistance, do not hesitate to reach out to the repository maintainers.

Thank you for your dedication and happy coding!